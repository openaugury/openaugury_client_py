from setuptools import setup, find_packages
from src.openaugury.brand import BRAND, CLI, URL, DESCRIPTION

cmdclass = {}
try:
    from babel.messages import frontend as babel
    cmdclass.update({
        'compile_catalog': babel.compile_catalog,
        'extract_messages': babel.extract_messages,
        'init_catalog': babel.init_catalog,
        'update_catalog': babel.update_catalog,
    })
except ImportError as e:
    pass

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError as e:
    pass

name = BRAND
version = '0.1'
release = '0.1.1'
setup(
    name=name,
    version=release,
    description=DESCRIPTION,
    url=URL,
    author='Flax & Teal Limited',
    author_email='phil.weir@flaxandteal.co.uk',
    license='AGPL',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Researchers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.8'
    ],
    keywords='disasters phenomena simulation modelling mathematics training',
    setup_requires=['pytest-runner'],
    extras_require={
        'examples': [],
        'babel-commands': ['Babel'],
        'sphinx-commands': ['sphinx'],
        'develop': [
            'pylint'
        ]
    },
    install_requires=[
        'pyyaml',
        'Click<7.0',
        'dateparser',
        'colorama',
        'requests',
        'docker',
        'jsonapi_client',
        'tabulate',
        'retry'
    ],
    include_package_data=True,
    tests_require=[
        'pytest',
        'pytest-asyncio',
        'mock',
        'asynctest'
    ],
    entry_points=f'''
        [console_scripts]
        {CLI}=openaugury.scripts.openaugury_whitelabel_client_py:cli
    ''',
    cmdclass=cmdclass,
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', release)
        }
    }
)
