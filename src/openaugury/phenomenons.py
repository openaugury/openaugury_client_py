"""
Controller for OpenAugury phenomenons

GNU AGPLv3
"""

from tabulate import tabulate


class Phenomenon:
    def __init__(self, idx, name):
        self.id = idx
        self.name = name

    def who_am_i(self, printer):
        return f'{self.name} [{printer.fmtid(self.id)}]'

    @classmethod
    def from_resource(cls, res, hyd=True):
        return cls(res.id, res.name)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class PhenomenonController:
    def __init__(self, api):
        self._api = api

    def get(self):
        phenomenons = self._api.index('phenomenons')
        return Phenomenon.from_resources(phenomenons.resources)


class PhenomenonView:
    def __init__(self):
        pass

    def table(self, phenomenons):
        headers = [
            _('ID'),
            _('Name')
        ]
        rows = []

        for phenomenon in phenomenons:
            rows.append([
                phenomenon.id,
                phenomenon.name
            ])
        table = tabulate(rows, headers=headers)

        return table
