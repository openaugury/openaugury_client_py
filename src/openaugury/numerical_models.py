"""
Controller for OpenAugury numerical_models

GNU AGPLv3
"""

from tabulate import tabulate
from .config import cache
from .phenomenons import Phenomenon


class NumericalModel:
    def __init__(self, idx, name, phenomenon, definition=None):
        self.id = idx
        self.name = name
        self.phenomenon = phenomenon
        self.definition = definition

    def who_am_i(self, printer):
        return f'{self.name} [{printer.fmtid(self.id)}]'

    @classmethod
    def from_resource(cls, res, hyd=True):
        phenomenon = None

        if hyd:
            if hasattr(res, 'phenomenon'):
                phenomenon = Phenomenon.from_resource(res.phenomenon, False)
                cache.add('phenomenons', phenomenon.id)

        if hasattr(res, 'definition'):
            definition = res.definition
        else:
            definition = None

        cache.add('numericalModels', res.id)

        return cls(res.id, res.name, phenomenon, definition)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class NumericalModelController:
    def __init__(self, api):
        self._api = api

    def create(self, name, phenomenon_key, definition):
        phenomenon = self._api.show('phenomenons', phenomenon_key).resource
        numerical_model = self._api.create(
            'numericalModels',
            include=['phenomenon'],
            name=name,
            phenomenon=phenomenon,
            definition=definition
        )
        return NumericalModel.from_resource(numerical_model.resource)

    def get(self, idx=None):
        if idx is None:
            numerical_models = self._api.index('numericalModels', include=['phenomenon'])
            return NumericalModel.from_resources(numerical_models.resources)
        else:
            numerical_model = self._api.show('numericalModels', idx, include=['phenomenon'])
            return NumericalModel.from_resource(numerical_model.resource)


class NumericalModelView:
    def __init__(self, printer):
        self._printer = printer

    def show(self, numerical_model):
        p = self._printer
        rows = [
            ('NUMERICAL MODEL', numerical_model.who_am_i(p)),
            (_('Phenomenon'), numerical_model.phenomenon.who_am_i(p) if numerical_model.phenomenon else _('(none)')),
            (_('Definition'), numerical_model.definition)
        ]
        table = tabulate(rows)

        return table

    def table(self, numerical_models):
        p = self._printer
        headers = [
            _('NUMERICAL MODEL'),
            _('Phenomenon')
        ]
        rows = []

        for numerical_model in numerical_models:
            rows.append([
                numerical_model.who_am_i(p),
                numerical_model.phenomenon.who_am_i(p) if numerical_model.phenomenon else _('(none)')
            ])
        table = tabulate(rows, headers=headers)

        return table
