DEFAULT_FULL_IDS = False
DEFAULT_ELLIPSIZE_LENGTH = 8

class Printer:
    def __init__(self, options):
        self.set_options(options)

    def set_options(self, options):
        self._options = options
        self._full_ids = self._options.get('fullIds', DEFAULT_FULL_IDS)
        self._ell_ct = self._options.get('ellipsizeLength', DEFAULT_ELLIPSIZE_LENGTH)

    def fmtid(self, idx):
        if self._full_ids:
            return idx
        else:
            return self.ell(idx)

    def ell(self, text):
        return text[:self._ell_ct - 3] + '...' if len(text) > self._ell_ct else text
