"""
Controller for OpenAugury case_contexts

GNU AGPLv3
"""

from tabulate import tabulate


class CaseContext:
    def __init__(self, idx, name):
        self.id = idx
        self.name = name

    def who_am_i(self, printer):
        return f'{self.name} [{printer.fmtid(self.id)}]'

    @classmethod
    def from_resource(cls, res, hyd=True):
        return cls(res.id, res.name)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class CaseContextController:
    def __init__(self, api):
        self._api = api

    def get(self, idx=None):
        if idx is None:
            case_contexts = self._api.index('caseContexts')
            return CaseContext.from_resources(case_contexts.resources)
        else:
            case_context = self._api.show('simulations', idx)
            return CaseContext.from_resource(case_context.resource)


class CaseContextView:
    def __init__(self, printer):
        self._printer = printer

    def show(self, case_context):
        p = self._printer
        rows = [
            ('CASE CONTEXT (Target Zone)', case_context.who_am_i(p))
        ]
        table = tabulate(rows)

        return table

    def table(self, case_contexts):
        p = self._printer
        headers = [
            _('ID'),
            _('Name')
        ]
        rows = []

        for case_context in case_contexts:
            rows.append([
                case_context.who_am_i(p),
                case_context.name
            ])
        table = tabulate(rows, headers=headers)

        return table
