"""
Configuration for OpenAugury Python client framework

NU AGPLv3
"""

import click
import json
import gettext
import logging
import dateparser
import getpass
import colorama
from functools import update_wrapper
from openaugury import commands
from openaugury.config import cache, config as init_config
from openaugury.brand import SERVER, BRAND
from openaugury.api import Api
from openaugury.print import Printer
from openaugury.simulations import SimulationController, SimulationView
from openaugury.combinations import CombinationController, CombinationView
from openaugury.case_contexts import CaseContextController, CaseContextView
from openaugury.numerical_models import NumericalModelController, NumericalModelView

LOGGING_FORMAT = '%(asctime)-15s %(message)s'

def context(f):
    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        result = ctx.invoke(f, ctx, *args, **kwargs)
        ctx.obj['config'].save()
        cache.save()
        return result
    return update_wrapper(new_func, f)

@click.group()
@click.option('--debug/--no-debug', default=False)
@click.option('--full-ids/--no-full-ids', '-i', default=None)
@context
def cli(ctx, debug, full_ids):
    gettext.install('ltldoorstep')
    colorama.init()

    init_config.load()
    cache.load()

    if full_ids is not None:
        init_config.set('print.fullIds', full_ids, permanent=False)

    view_options = {
        'full_ids'
    }

    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO, format=LOGGING_FORMAT)
    logger = logging.getLogger(__name__)

    json_logger = logging.getLogger('jsonapi_client')
    json_logger.setLevel(logging.ERROR)

    printer = Printer(init_config.view('print', {}))

    ctx.obj = {
        'DEBUG': debug,
        'config': init_config,
        'logger': logger,
        'printer': printer,
        'api': Api(init_config)
    }

def welcome_message():
    msg = BRAND + ': ' + _('CLI Tool')
    return msg + '\n' + '=' * len(msg)

@cli.command()
@context
def login(ctx):
    click.echo(welcome_message())
    click.echo(f'Logging into {SERVER}' )

    config = ctx.obj['config']

    api_token = input('API Token: ')

    config.set('api.token', api_token)

    config.save()

@cli.group()
@context
def numericalmodels(ctx):
    ctx.obj['controller'] = NumericalModelController(ctx.obj['api'])
    ctx.obj['view'] = NumericalModelView(ctx.obj['printer'])

@cli.group()
@context
def simulations(ctx):
    ctx.obj['controller'] = SimulationController(ctx.obj['api'])
    ctx.obj['view'] = SimulationView(ctx.obj['printer'])

@cli.group()
@context
def casecontexts(ctx):
    ctx.obj['controller'] = CaseContextController(ctx.obj['api'])
    ctx.obj['view'] = CaseContextView(ctx.obj['printer'])

@cli.group()
@context
def combinations(ctx):
    ctx.obj['controller'] = CombinationController(ctx.obj['api'])
    ctx.obj['view'] = CombinationView(ctx.obj['printer'])

def generic_action(ctx, action, idx, options={}):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.action(controller, view, action, idx, options)

def generic_get(ctx, idx):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.get(controller, view, idx)

def generic_make(ctx, **attributes):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.make(controller, view, attributes)

@casecontexts.command()
@click.argument('idx', nargs=-1)
@context
def get(ctx, idx):
    idx = idx[0] if idx else None

    generic_get(ctx, idx)

@simulations.command()
@click.argument('idx')
@context
def open(ctx, idx):
    generic_action(ctx, 'open', idx)

@combinations.command()
@click.argument('idx', nargs=-1)
@context
def get(ctx, idx):
    idx = idx[0] if idx else None

    generic_get(ctx, idx)

@combinations.command()
@click.option('--numerical-model', '-m', required=True)
@click.option('--status', '-s', default='ACTIVE')
@context
def make(ctx, numerical_model, status):
    generic_make(ctx, status=status, numerical_model_key=numerical_model)

@simulations.command()
@click.option('--combination', '-c', required=True)
@click.option('--case-context', '-C', required=True)
@click.option('--begins', '-b', default=None)
@click.option('--ends', '-e', default=None)
@context
def make(ctx, combination, case_context, begins, ends):
    if begins:
        begins = dateparser.parse(begins)
    if ends:
        ends = dateparser.parse(begins)

    generic_make(ctx, begins=begins, ends=ends, combination_key=combination, case_context_key=case_context)

@numericalmodels.command()
@click.option('--phenomenon', '-p', required=True)
@click.option('--definition', '-d', default=None)
@click.option('--type', '-t', default=None)
@click.argument('name')
@context
def make(ctx, name, phenomenon, definition, type):
    if definition is None and type is not None:
        definition = json.dumps({
            'type': type,
            'parameters': {}
        })
    generic_make(ctx, name=name, phenomenon_key=phenomenon, definition=definition)

@simulations.command()
@click.argument('idx', nargs=-1)
@context
def get(ctx, idx):
    idx = idx[0] if idx else None

    generic_get(ctx, idx)

@simulations.command()
@click.option('--force/--no-force', '-f', default=False)
@click.argument('idx')
@context
def run(ctx, idx, force):
    options = {}

    if force:
        options['force'] = True

    generic_action(ctx, 'run', idx, options)

@numericalmodels.command()
@click.argument('idx', nargs=-1)
@context
def get(ctx, idx):
    idx = idx[0] if idx else None

    generic_get(ctx, idx)
