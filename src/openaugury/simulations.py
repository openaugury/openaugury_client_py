"""
Controller for OpenAugury simulations

GNU AGPLv3
"""

from enum import Enum
from colorama import Fore, Style
from tabulate import tabulate
from datetime import datetime, timedelta
import webbrowser
from jsonapi_client import Inclusion
from .case_contexts import CaseContext
from .numerical_models import NumericalModel
from .phenomenons import Phenomenon
from .config import cache, Undefined
from .brand import SERVER


class Simulation:
    class Status(Enum):
        INACTIVE = 0
        UNPROCESSED = 1
        IN_PROGRESS = 2
        COMPLETE = 3
        FAILED = 4

        @property
        def colored(self):
            cmap = {
                self.COMPLETE:    Fore.GREEN,
                self.UNPROCESSED: Fore.YELLOW,
                self.IN_PROGRESS: Fore.MAGENTA,
                self.FAILED:      Fore.RED,
            }
            return (cmap[self] + self.name + Style.RESET_ALL) if self in cmap else self.name

    def __init__(self, idx, status, numerical_model, phenomenon, case_context):
        self.id = idx
        self.status = self.Status(status)
        self.numerical_model = numerical_model
        self.phenomenon = phenomenon
        self.case_context = case_context

    def who_am_i(self, printer):
        return printer.fmtid(self.id)

    @classmethod
    def from_resource(cls, res, hyd=True):
        case_context = None
        numerical_model = None
        phenomenon = None

        if hyd:
            if hasattr(res, 'numericalModel'):
                numerical_model = NumericalModel.from_resource(res.numericalModel, False)
                cache.add('numericalModels', numerical_model.id)

            if hasattr(res, 'caseContext'):
                case_context = CaseContext.from_resource(res.caseContext, False)
                cache.add('caseContexts', case_context.id)

            if hasattr(res, 'phenomenon'):
                phenomenon = Phenomenon.from_resource(res.phenomenon, False)
                cache.add('phenomenons', phenomenon.id)

        cache.add('simulations', res.id)

        return cls(res.id, res.status, numerical_model, phenomenon, case_context)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class SimulationController:
    def __init__(self, api):
        self._api = api

    def run(self, idx, force=False):
        include = ['numericalModel', 'caseContext', 'phenomenon']
        simulation = self._api.action('simulations', idx, 'run', include=include, options={'force': 1 if force else 0})
        return Simulation.from_resource(simulation.resource)

    def open(self, idx):
        include = ['numericalModel', 'caseContext', 'phenomenon']
        simulation = self._api.action('simulations', idx, 'run', include=include)

        simulation = Simulation.from_resource(simulation.resource)

        webbrowser.open(f'{SERVER}/v/#/s/{simulation.id}')

        return simulation

    def create(self, begins, ends, combination_key, case_context_key):
        if begins is None:
            begins = datetime.now()
        if ends is None:
            ends = begins + timedelta(hours=2)

        begins = begins.isoformat()
        ends = ends.isoformat()

        combination = self._api.show('combinations', combination_key).resource
        case_context = self._api.show('caseContexts', case_context_key).resource
        simulation = self._api.create(
            'simulations',
            include=['caseContext', 'numericalModel', 'phenomenon'],
            begins=begins,
            ends=ends,
            caseContext=case_context,
            combination=combination
        )
        return Simulation.from_resource(simulation.resource)

    def get(self, idx=None):
        if idx is None:
            cache.forget('simulations')
            simulations = self._api.index('simulations', include=['numericalModel', 'caseContext', 'phenomenon'])
            return Simulation.from_resources(simulations.resources)
        else:
            simulation = self._api.show('simulations', idx, include=['numericalModel', 'caseContext', 'phenomenon'])
            return Simulation.from_resource(simulation.resource)


class SimulationView:
    def __init__(self, printer):
        self._printer = printer

    def show(self, simulation):
        p = self._printer
        rows = [
            ('SIMULATION', simulation.who_am_i(p)),
            (_('Status'), simulation.status.colored),
            (_('Phenomenon'), simulation.phenomenon.who_am_i(p) if simulation.phenomenon else _('(none)')),
            (_('Target Zone (Case Context)'), simulation.case_context.who_am_i(p) if simulation.case_context else _('(none)')),
            (_('Model'), simulation.numerical_model.who_am_i(p) if simulation.numerical_model else _('(none)'))
        ]
        table = tabulate(rows)

        return table

    def table(self, simulations):
        p = self._printer
        headers = [
            _('SIMULATION'),
            _('Status'),
            _('Phenomenon'),
            _('Target Zone (Case Context)'),
            _('Model')
        ]
        rows = []

        for simulation in simulations:
            rows.append([
                simulation.who_am_i(p),
                simulation.status.colored,
                simulation.phenomenon.who_am_i(p) if simulation.phenomenon else _('(none)'),
                simulation.case_context.who_am_i(p) if simulation.case_context else _('(none)'),
                simulation.numerical_model.who_am_i(p) if simulation.numerical_model else _('(none)')
            ])
        table = tabulate(rows, headers=headers)

        return table
