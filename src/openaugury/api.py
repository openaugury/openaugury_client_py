"""
API Controller for OpenAugury

GNU AGPLv3
"""

import requests
from jsonapi_client import Session, Inclusion, Modifier
from .config import cache, Undefined
from .brand import SERVER

class InclusionWithId(Inclusion):
    def __init__(self, idx, *include_args: 'IncludeKeywords') -> None:
        self._idx = idx
        Inclusion.__init__(self, *include_args)

    def url_with_modifiers(self, base_url: str) -> str:
        base_url = f'{base_url}/{self._idx}'
        return Inclusion.url_with_modifiers(self, base_url)

models_as_jsonschema = {
    'simulations': {'properties': {
        'combination': {'relation': 'to-one', 'resource': ['combinations']},
        'caseContext': {'relation': 'to-one', 'resource': ['caseContexts']},
        'numericalModel': {'relation': 'to-one', 'resource': ['numericalModels']},
        'phenomenon': {'relation': 'to-one', 'resource': ['phenomenons']}
    }},
    'combinations': {'properties': {
        'status': {'type': 'integer'},
        'numericalModel': {'relation': 'to-one', 'resource': ['numericalModels']},
        'phenomenon': {'relation': 'to-one', 'resource': ['phenomenons']}
    }},
    'numericalModels': {'properties': {
        'name': {'type': 'string'},
        'definition': {'type': 'string'},
        'phenomenon': {'relation': 'to-one', 'resource': ['phenomenons']}
    }}
}

class Api:
    def __init__(self, config):
        self._config = config

        self.start_session()

    def start_session(self):
        root = SERVER
        prefix = self._config.get('api.prefix')
        token = self._config.get('api.token')
        self._session = Session(
            f'{root}{prefix}',
            schema=models_as_jsonschema,
            request_kwargs={'headers': {'Authorization': f'Bearer {token}'}}
        )

    def index(self, resource, include=None):
        if include:
            inclusion = Inclusion(*include)
        else:
            inclusion = None

        return self._session.get(resource, inclusion)

    def create(self, resource, include=None, **attributes):
        entity = self._session.create(resource)

        for key, value in attributes.items():
            setattr(entity, key, value)

        entity.commit()

        idx = entity.id
        if include:
            inclusion = InclusionWithId(idx, *include)
        else:
            inclusion = idx

        entity = self._session.get(resource, inclusion)

        return entity

    def show(self, resource, idx, include=None):
        cidx = cache.find(resource, idx)
        if cidx is not Undefined:
            idx = cidx[1]['key']

        if include:
            inclusion = InclusionWithId(idx, *include)
        else:
            inclusion = idx

        # FIXME: why can't we use include with an id??
        return self._session.get(resource, inclusion)

    def action(self, resource, idx, action, include=None, options={}):
        cidx = cache.find(resource, idx)
        if cidx is not Undefined:
            idx = cidx[1]['key']

        entity = self._session.get(resource, idx)
        url = entity.resource.url
        query = ''
        if options:
            query = '?' + '&'.join(f'{k}={v}' for k, v in options.items())

        token = self._config.get('api.token')
        headers = {'Authorization': f'Bearer {token}'}
        requests.post(f'{url}/{action}{query}', headers=headers)

        return self.show(resource, idx, include)
