
def get(controller, view, idx=None):
    if idx is not None:
        entity = controller.get(idx)
        print(view.show(entity))
    else:
        entities = controller.get()
        print(view.table(entities))

def action(controller, view, action, idx=None, options={}):
    entity = getattr(controller, action)(idx, **options)
    if isinstance(entity, list):
        print(view.table(entity))
    else:
        print(view.show(entity))

def make(controller, view, attributes):
    entity = controller.create(**attributes)
    print(view.show(entity))
