"""
Controller for OpenAugury combinations

GNU AGPLv3
"""

from jsonapi_client import Inclusion
from tabulate import tabulate
from .numerical_models import NumericalModel
from .phenomenons import Phenomenon
from .config import cache, Undefined
from enum import Enum
from colorama import Fore, Style


class Combination:
    class Status(Enum):
        INACTIVE = 0
        ACTIVE   = 1
        PRIVATE  = 2

        @property
        def colored(self):
            cmap = {
                self.INACTIVE:    Fore.BLUE,
                self.ACTIVE:      Fore.GREEN,
                self.PRIVATE:     Fore.MAGENTA
            }
            return (cmap[self] + self.name + Style.RESET_ALL) if self in cmap else self.name

    def __init__(self, idx, status, numerical_model, phenomenon):
        self.id = idx
        self.status = self.Status(status)
        self.numerical_model = numerical_model
        self.phenomenon = phenomenon

    def who_am_i(self, printer):
        return printer.fmtid(self.id)

    @classmethod
    def from_resource(cls, res, hyd=True):
        numerical_model = None
        phenomenon = None

        if hyd:
            if hasattr(res, 'numericalModel'):
                numerical_model = NumericalModel.from_resource(res.numericalModel, False)
                cache.add('numericalModels', numerical_model.id)

            if hasattr(res, 'phenomenon'):
                phenomenon = Phenomenon.from_resource(res.phenomenon, False)
                cache.add('phenomenons', phenomenon.id)

        cache.add('combinations', res.id)

        return cls(res.id, res.status, numerical_model, phenomenon)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class CombinationController:
    def __init__(self, api):
        self._api = api

    def run(self, idx, force=False):
        include = ['numericalModel', 'phenomenon']
        combination = self._api.action('combinations', idx, 'run', include=include, options={'force': 1})
        return Combination.from_resource(combination.resource)

    def create(self, status, numerical_model_key):
        numerical_model = self._api.show('numericalModels', numerical_model_key).resource
        status = Combination.Status[status].value
        combination = self._api.create(
            'combinations',
            include=['numericalModel', 'phenomenon'],
            status=status,
            numericalModel=numerical_model
        )
        return Combination.from_resource(combination.resource)

    def get(self, idx=None):
        if idx is None:
            cache.forget('combinations')
            combinations = self._api.index('combinations', include=['numericalModel', 'phenomenon'])
            return Combination.from_resources(combinations.resources)
        else:
            combination = self._api.show('combinations', idx, include=['numericalModel', 'phenomenon'])
            return Combination.from_resource(combination.resource)


class CombinationView:
    def __init__(self, printer):
        self._printer = printer

    def show(self, combination):
        p = self._printer
        rows = [
            ('COMBINATION', combination.who_am_i(p)),
            (_('Status'), combination.status.colored),
            (_('Phenomenon'), combination.phenomenon.who_am_i(p) if combination.phenomenon else _('(none)')),
            (_('Model'), combination.numerical_model.who_am_i(p) if combination.numerical_model else _('(none)'))
        ]
        table = tabulate(rows)

        return table

    def table(self, combinations):
        p = self._printer
        headers = [
            _('COMBINATION'),
            _('Status'),
            _('Phenomenon'),
            _('Model')
        ]
        rows = []

        for combination in combinations:
            rows.append([
                combination.who_am_i(p),
                combination.status.colored,
                combination.phenomenon.who_am_i(p) if combination.phenomenon else _('(none)'),
                combination.numerical_model.who_am_i(p) if combination.numerical_model else _('(none)')
            ])
        table = tabulate(rows, headers=headers)

        return table
