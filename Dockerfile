FROM python:3.6

COPY requirements.txt /

RUN pip3 install -r /requirements.txt

COPY . /openaugury_client_py

RUN adduser openaugury

WORKDIR /openaugury_client_py

RUN python3 setup.py develop

USER openaugury

ENTRYPOINT [ "/openaugury/run.sh" ]
